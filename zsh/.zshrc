# Mein zshrc von Grund auf selbst geschrieben
# 28.02.2019 gestartet

export LC_CTYPE="de_DE.UTF8"

## Pathes
fpath=($fpath $HOME/.config/zshFunctions)
# golang environemnt
export GOPATH=$HOME/opi/go
export GOBIN=$HOME/opi/go/bin
export PATH=$PATH:$HOME/opi/go/bin
export QT_QPA_PLATFORMTHEME="qt5ct"

## Prompt
#export PROMPT="%n@%m: [%(4c.%1c.%~)]%# "
#export RPROMPT="%(0?..:() %T"
autoload -Uz promptinit
promptinit
prompt spaceship


## Aliases
alias ls='ls --color'
alias nv='/usr/bin/nvim'
alias ta='/usr/bin/textadept --nosession'
alias now='date +"%d.%m.%Y %H:%M:%S"'
alias history='history 0'

### alias für dotfile-repository
# alias dot='/usr/bin/git --git-dir=$HOME/.dotfiles --work-tree=$HOME'


## History
export HISTFILE=$HOME/.zsh_history
export HISTSIZE=1000
export SAVEHIST=1000
# setopt EXTENDED_HISTORY	# speichert zu jedem Kommando die Startzeit und die Dauer: Startzeit in Sekunden:Dauer in Sekunden:Kommando
setopt HIST_IGNORE_DUPS		# speichert einen eingegebenen Befehl nur einmal
setopt HIST_FIND_NO_DUPS	# wenn man in der Historie sucht, dann werden gleiche Kommandos nicht aufgelistet, zB wenn ls 20 mal auf gerufen wurde, wird es nur einmal angezeigt)
setopt INC_APPEND_HISTORY	# wie APPEND_HISTORY, nur, dass die neuen Befehle gleich in die Historie aufgenommen werden und nicht erst wenn die shell verlassen wird

## Completition
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select	# hiermit kann ich mit den Pfeiltasten durch ein Menü navigieren
zmodload zsh/complist
_comp_options+=(globdots)

## others
# setopt autocd	# wenn bei einer Eingabe kein programm für die Auto-Completition gefunden wurde, dann wird versucht den Verzeichnissnamen zu ergänzen

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' 'lfcd\n'

# Keybind
# Um die KeyCodes heruaszufinden in einem ZSH-Terminal <Strg>+<v> und dann die Tasten(-kombination) drücken
#bindkey '^[OC' forward-word
#bindkey '^[Oc' forward-word
bindkey '^[[1;5C' forward-word
#bindkey '^[OD' backward-word
#bindkey '^[Od' backward-word
bindkey '^[[1;5D' backward-word
#bindkey '^[[1~' beginning-of-line
#bindkey '^[[7~' beginning-of-line
#bindkey '^[OH' beginning-of-line
#bindkey '\033[H' beginning-of-line
#bindkey '^[[1;6D' beginning-of-line
#bindkey '^[[8~' end-of-line
#bindkey '^[[4~' end-of-line
#bindkey '^[OF' end-of-line
#bindkey '\033[F' end-of-line
#bindkey '^[[1;6C' end-of-line
bindkey '^[[3~' delete-char	# Entf-Taste
bindkey '^[[H' beginning-of-line	# Pos1 Taste
bindkey '^[[F' end-of-line	# Ende Taste

# load zsh-syntax-highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
---------------------------------------------------------------------------------------------------------------------------------
-- functions
---------------------------------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------------------------------
-- fonts, theming etc.
---------------------------------------------------------------------------------------------------------------------------------
--buffer:set_theme('base16-summerfruit-light', { font = "Fira Mono Regular", fontsize = 12})
buffer:set_theme('base16-gruvbox-dark', { font = "Fira Mono Regular", fontsize = 14})
buffer.caret_fore = 0x0000FF
buffer.caret_width = 2
buffer.caret_line_frame = 1
--buffer.caret_line_back = 0x556677
--buffer:set_sel_back(true,0x778899)

---------------------------------------------------------------------------------------------------------------------------------
-- editor settings
---------------------------------------------------------------------------------------------------------------------------------
buffer.use_tabs = true
buffer.tab_width = 4
-- make whitespaces visible:
buffer.view_ws = buffer.WS_VISIBLEALWAYS

-- The amount of pixel padding above lines. The default value is 0.
buffer.extra_ascent = 3
-- The amount of pixel padding below lines. The default is 0.
buffer.extra_descent = 3

-- buffer.wrap_visual_flags = buffer.WRAPVISUALFLAGLOC_START_BY_TEXT

-- buffer.wrap_mode = buffer.WRAP_WHITESPACE

-- Line Endings
buffer.view_eol = false


---------------------------------------------------------------------------------------------------------------------------------
-- keybindings
---------------------------------------------------------------------------------------------------------------------------------
keys['c+'] = function()
	buffer.zoom = buffer.zoom + 1
end

-- insert new line after current line
keys['cl'] = function()
	buffer:line_end()
	buffer:new_line()
end

-- insert new line before current line
keys['cL'] = function()
	buffer:home()
	buffer:new_line()
	buffer:line_up()
end

-- delet current line
keys['cd'] = function()
	buffer:line_delete()
end
